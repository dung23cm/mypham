package myphamseoul.dto;

import java.util.ArrayList;
import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import myphamseoul.base.DtoBase;

@Data
@EqualsAndHashCode(callSuper = false)
@NoArgsConstructor
@AllArgsConstructor
public class ProductGeneral extends DtoBase{
	private String id;
	private Long brandId;
	private String brandName;
	private String lineProductName;
	private String categoryName;
	private String sku;
	private String name;
	private String shortDescription;
	private String description;
	private int sale;
	private long price;
	private List<String> tags = new ArrayList<String>();
}
