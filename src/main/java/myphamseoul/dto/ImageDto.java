package myphamseoul.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import myphamseoul.base.DtoBase;
import myphamseoul.entity.ProductColorKey;

@Data
@EqualsAndHashCode(callSuper = false)
@AllArgsConstructor
@NoArgsConstructor
public class ImageDto extends DtoBase{
	private String id;
	private String name;
	private ProductColorKey productId;
}
