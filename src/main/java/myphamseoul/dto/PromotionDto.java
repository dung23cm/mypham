package myphamseoul.dto;

import java.util.Date;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import myphamseoul.base.DtoBase;
@Data
@EqualsAndHashCode(callSuper = false)
@NoArgsConstructor
@AllArgsConstructor
public class PromotionDto extends DtoBase{
	private Long id;
	private String name;
	private String description;
	private Date locationIn;
	private Date locationOut;
	private int sale;
}
