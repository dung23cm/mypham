package myphamseoul.dto;

import java.util.ArrayList;
import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import myphamseoul.base.DtoBase;

@Data
@EqualsAndHashCode(callSuper = false)
@AllArgsConstructor
@NoArgsConstructor
public class BrandDto extends DtoBase{
	private Long id;
	private String name;
	List<String> lineProducts = new ArrayList<String>();
}
