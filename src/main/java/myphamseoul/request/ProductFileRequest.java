package myphamseoul.request;

import org.springframework.web.multipart.MultipartFile;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import myphamseoul.dto.ProductDto;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ProductFileRequest {
	ProductDto product;
	MultipartFile fileImg;
}
