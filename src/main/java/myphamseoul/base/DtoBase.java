package myphamseoul.base;

import java.time.LocalDateTime;

import lombok.Data;

@Data
public class DtoBase {
	private String createdBy;
	private LocalDateTime createdWhen;
	private String updateBy;
	private LocalDateTime updateWhen;
}
