package myphamseoul.base;

import java.util.List;

public interface ServiceBase<E,I> {
	List<E> getAll();
	E getById(I id);
	E save(E entity);
	boolean existById(I id);
	void delete(I id);
	void delete(I[] ids);
}
