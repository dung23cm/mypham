package myphamseoul.base;

import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;

import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedBy;
import org.springframework.data.annotation.LastModifiedDate;

import lombok.Data;


@Data
@MappedSuperclass
public class EntityBase {
	@Column(updatable = false, length = 50)
	@CreatedBy
	private String createdBy;
	
	@Column(updatable = false)
	@CreatedDate
	private LocalDateTime createdWhen;
	
	@Column(length = 50)
	@LastModifiedBy
	private String updateBy;
	
	@Column
	@LastModifiedDate
	private LocalDateTime updateWhen;
}
