package myphamseoul.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Embeddable
@Data
@AllArgsConstructor
@NoArgsConstructor
public class ProductColorKey implements Serializable{
	private static final long serialVersionUID = 1L;
	@Column(name = "productmain_id")
	private String productMainId;
	@Column(name = "color_id")
	private Long colorId;
}
