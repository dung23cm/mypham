package myphamseoul.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import myphamseoul.base.EntityBase;

@Entity
@Data
@EqualsAndHashCode(callSuper = false)
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "image")
public class ImageEntity extends EntityBase{
	@Id
	@GeneratedValue(generator = "uuid2")
	@GenericGenerator(name = "uuid2",strategy = "org.hibernate.id.UUIDGenerator")
	private String id;
	@Column(unique = true)
	private String name;
	@ManyToOne
	@JoinColumns(value = {@JoinColumn(name = "color_id"),@JoinColumn(name = "productmain_id")})
	private ProductEntity product;
	private boolean main;
	private boolean hover;
	private boolean draft;
}
