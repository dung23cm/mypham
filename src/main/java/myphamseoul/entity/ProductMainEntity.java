package myphamseoul.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.GenericGenerator;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "product_main")
public class ProductMainEntity {
	@Id
	@GeneratedValue(generator = "uuid2")
	@GenericGenerator(name = "uuid2",strategy = "org.hibernate.id.UUIDGenerator")
	private String id;
	
	@Column(length = 255)
	private String name;
	
	@NotNull
	@NotBlank
	@Column(length = 50, unique = true)
	private String sku;
	
	@Column(length = 255)
	private String shortDescription;
	
	@Column(columnDefinition = "TEXT")
	private String description;
	
	@ManyToOne
	@JoinColumns(value = {@JoinColumn(name = "brand_id"),@JoinColumn(name = "lineproduct_id")})
	private BrandLineProductEntity brandLineProduct;
	
	@ManyToOne
	@JoinColumn(name = "category_id",referencedColumnName = "id")
	private CategoryEntity category;
}
