package myphamseoul.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import myphamseoul.dto.TagDto;
import myphamseoul.mapper.TagMapper;
import myphamseoul.response.MessageResponse;
import myphamseoul.service.TagService;

@RestController
@RequestMapping("/api/v1/tag")
public class TagController {
	
	private static final Logger log = LoggerFactory.getLogger(TagController.class);

	@Autowired
	private TagService tagService;
	@Autowired
	private TagMapper tagMapper;
	@PutMapping
	public ResponseEntity<?> putTag(@RequestBody TagDto dto) {
		log.info("put tag");
		dto.setName(dto.getName().trim());
		if(dto.getId()==null) {
			if(tagService.findOneByName(dto.getName())!=null) {
				return ResponseEntity.ok().body(new MessageResponse("Tag already exist!"));
			}
		}
		return ResponseEntity.ok().body(tagMapper.toDto(tagService.save(tagMapper.toEntity(dto))));
	}

}
