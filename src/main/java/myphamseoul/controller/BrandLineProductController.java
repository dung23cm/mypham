package myphamseoul.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import myphamseoul.dto.BrandLineProductDto;
import myphamseoul.mapper.BrandLineProductMapper;
import myphamseoul.service.BrandLineProductService;

@RestController
@RequestMapping("/api/v1/brand-lineproduct")
public class BrandLineProductController {
	
	private static final Logger log = LoggerFactory.getLogger(BrandLineProductController.class);

	@Autowired
	private BrandLineProductService brandLineProductService;
	@Autowired
	private BrandLineProductMapper brandLineProductMapper;
	@PutMapping
	public ResponseEntity<?> putBrandLineProduct(@RequestBody BrandLineProductDto dto) {
		//TODO: process POST request
		log.info("put brand-lineproduct");
		return ResponseEntity.ok().body(brandLineProductMapper.toDto(brandLineProductService.save(dto)));
	}
	
	@GetMapping(value = "/{brandId}/{lineProductId}")
	public ResponseEntity<?> getByBrandIdAndLineProductId(@PathVariable Long brandId,@PathVariable Long lineProductId) {
		log.info("get By BrandId: "+brandId+" And LineProductId: "+lineProductId);
		return ResponseEntity.ok().body(brandLineProductMapper.toDto(brandLineProductService.findByBrandIdAndLineProductId(brandId, lineProductId)));
	}
	
	@GetMapping(value = "/all")
	public ResponseEntity<?> getBrandLineProductAll() {
		log.info("getBrandLineProductAll");
		return ResponseEntity.ok().body(brandLineProductMapper.toDto(brandLineProductService.getAll()));
	}

}
