package myphamseoul.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import myphamseoul.dto.BrandDto;
import myphamseoul.mapper.BrandMapper;
import myphamseoul.response.MessageResponse;
import myphamseoul.service.BrandLineProductService;
import myphamseoul.service.BrandService;

@RestController
@RequestMapping("/api/v1/brand")
public class BrandController {
	
	private static final Logger log = LoggerFactory.getLogger(BrandController.class);

	@Autowired
	private BrandService brandService;
	
	@Autowired
	private BrandMapper brandMapper;
	
	@Autowired
	private BrandLineProductService brandLineProductService;
	
	@GetMapping("/all")
	public ResponseEntity<?> getBrandAll() {
		log.info("getBrandAll");
		return ResponseEntity.ok().body(brandMapper.toDto(brandService.getAll()));
	}
	
	@GetMapping("/by-lineproduct/{lineProductId}")
	public ResponseEntity<?> getBrandByLineProductId(@PathVariable Long lineProductId) {
		log.info("getBrandByLineProductId id: "+lineProductId);
		return ResponseEntity.ok().body(brandMapper.toDto(brandLineProductService.findBrandByLineProductId(lineProductId)));
	}

	@PutMapping()
	public ResponseEntity<?> putBrand(@RequestBody BrandDto dto) {
		dto.setName(dto.getName().trim());
		if(dto.getId()==null) {
			if(brandService.findOneByName(dto.getName())!=null) {
				return ResponseEntity.ok().body(new MessageResponse("Brand already exist!"));
			}
		}
		return ResponseEntity.ok().body(brandMapper.toDto(brandService.save(brandMapper.toEntity(dto))));
	}

}
