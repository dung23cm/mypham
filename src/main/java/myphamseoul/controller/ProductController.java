package myphamseoul.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import myphamseoul.dto.ProductDto;
import myphamseoul.dto.ProductMainDto;
import myphamseoul.entity.ProductEntity;
import myphamseoul.mapper.ProductMainMapper;
import myphamseoul.mapper.ProductMapper;
import myphamseoul.service.ProductMainService;
import myphamseoul.service.ProductService;

@RestController
@RequestMapping("/api/v1/product")
@CrossOrigin(value = "*")
public class ProductController {

	@Autowired
	private ProductMainService productMainService;

	@Autowired
	private ProductMainMapper productMainMapper;

	@Autowired
	private ProductService productService;

	@Autowired
	private ProductMapper productMapper;

	@PutMapping(value = "/main")
	public ResponseEntity<?> postMethodName(@RequestBody List<ProductMainDto> dto) {
		return ResponseEntity.ok().body(productMainMapper.toDto(productMainService.save(dto)));
	}

	@GetMapping("/all")
	public ResponseEntity<?> getMethodName() {
		return ResponseEntity.ok().body(productMainMapper.toDto(productMainService.getAll()));
	}

	@PostMapping
	public ResponseEntity<?> postProduct(@RequestBody ProductDto dto) {
		ProductEntity proE = productService.save(dto);
		return ResponseEntity.ok().body(productMapper.toDto(proE));
	}

}
