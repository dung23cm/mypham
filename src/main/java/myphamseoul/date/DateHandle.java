package myphamseoul.date;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.springframework.stereotype.Component;

@Component
public class DateHandle {
	public Date formatYYYYMMDD(String dateStr) {
		
		SimpleDateFormat formater = new SimpleDateFormat("YYYY-MM-DD");
		try {
			return formater.parse(dateStr);
		} catch (ParseException e) {
			return null;
		}
		
	}
}
