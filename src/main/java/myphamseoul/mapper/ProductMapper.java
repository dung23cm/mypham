package myphamseoul.mapper;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import myphamseoul.base.MapperBase;
import myphamseoul.dto.ColorDto;
import myphamseoul.dto.ProductDto;
import myphamseoul.dto.ProductMainDto;
import myphamseoul.dto.PromotionDto;
import myphamseoul.entity.ColorEntity;
import myphamseoul.entity.ImageEntity;
import myphamseoul.entity.ProductColorKey;
import myphamseoul.entity.ProductEntity;
import myphamseoul.entity.ProductMainEntity;
import myphamseoul.entity.PromotionEntity;
import myphamseoul.service.ColorService;
import myphamseoul.service.ImageService;
import myphamseoul.service.ProductMainService;
import myphamseoul.service.ProductPromotionService;
import myphamseoul.service.ProductService;

@Component
public class ProductMapper implements MapperBase<ProductEntity, ProductDto>{
	
	@Autowired
	private ProductService productService;
	
	@Autowired
	private ProductMainService productMainService;
	
	@Autowired
	private ProductMainMapper productMainMapper;
	
	@Autowired
	private ProductPromotionService productPromotionService;
	
	@Autowired
	private PromotionMapper promotionMapper;
	
	@Autowired
	private ColorService colorService;
	
	@Autowired
	private ColorMapper colorMapper;
	
	@Autowired
	private ImageService imageService;
	
	@Autowired
	private ImageMapper imageMapper;
	
	@Override
	public ProductEntity toEntity(ProductDto dto) {
		if(dto!=null) {
			ProductMainDto proMainDto = dto.getProductMain();
			ColorDto colorDto = dto.getColor();
			if(proMainDto!=null&& colorDto!=null) {
				if(colorDto.getId()!=null) {
					ProductMainEntity proMainE = null;
					if(proMainDto.getId()!=null
							&&proMainDto.getSku()!=null
							&&!proMainDto.getId().trim().equals("")
							&&!proMainDto.getSku().trim().equals("")) {
						ProductMainEntity proById = productMainService.getById(proMainDto.getId());
						ProductMainEntity proBySku = productMainService.findBySku(proMainDto.getSku());
						if(proById!=null&&proById.equals(proBySku)) {
							proMainE = proById;
						}else {
							return null;
						}
					}else if(proMainDto.getId()==null&&!proMainDto.getSku().trim().equals("")) {
						proMainE = productMainService.findBySku(proMainDto.getSku());
					}else if(proMainDto.getSku()==null&&!proMainDto.getId().trim().equals("")) {
						proMainE = productMainService.getById(proMainDto.getId());
					}
					proMainE = productMainService.getById(proMainDto.getId());
					ColorEntity colorE = colorService.getById(colorDto.getId());
					if(proMainE!=null&&colorE!=null) {
						return new ProductEntity(
								new ProductColorKey(proMainE.getId(),colorE.getId()), 
								proMainE, 
								colorE, 
								dto.getPrice());
					}
				}
				
			}
		}
		return null;
	}

	@Override
	public ProductDto toDto(ProductEntity entity) {
		if(entity!=null) {
			ProductMainEntity proMainE = entity.getProductMain();
			ColorEntity colorE = entity.getColor();
			ProductEntity proE = productService.getById(new ProductColorKey(proMainE.getId(), colorE.getId()));
			List<PromotionEntity> promEs = productPromotionService
					.findPromotionByProductProductMainIdAndProductColorId(proMainE.getId(), colorE.getId());
			ProductMainDto proMainDto = productMainMapper.toDto(proMainE);
			ColorDto colorDto = colorMapper.toDto(colorE);
			List<PromotionDto> promDtos = promotionMapper.toDto(promEs);
			List<ImageEntity> imageEs = imageService.findByProduct(proE);
			return new ProductDto(proMainDto, colorDto, null, promDtos, proE.getPrice(),imageMapper.toDto(imageEs), new ArrayList<>());
		}
		return null;
	}

	@Override
	public List<ProductEntity> toEntity(List<ProductDto> dtos) {
		List<ProductEntity> entitys = new ArrayList<>();
		for(ProductDto dto : dtos) {
			entitys.add(toEntity(dto));
		}
		return entitys;
	}

	@Override
	public List<ProductDto> toDto(List<ProductEntity> entitys) {
		List<ProductDto> dtos = new ArrayList<>();
		for(ProductEntity entity : entitys) {
			dtos.add(toDto(entity));
		}
		return dtos;
	}

}
