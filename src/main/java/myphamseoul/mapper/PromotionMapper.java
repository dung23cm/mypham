package myphamseoul.mapper;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Component;

import myphamseoul.base.MapperBase;
import myphamseoul.dto.PromotionDto;
import myphamseoul.entity.PromotionEntity;

@Component
public class PromotionMapper implements MapperBase<PromotionEntity, PromotionDto>{

	@Override
	public PromotionEntity toEntity(PromotionDto dto) {
		if(dto!=null) {
			return mapper.map(dto, PromotionEntity.class);
		}
		return null;
	}

	@Override
	public PromotionDto toDto(PromotionEntity entity) {
		if(entity!=null) {
			return mapper.map(entity, PromotionDto.class);
		}
		return null;
	}

	@Override
	public List<PromotionEntity> toEntity(List<PromotionDto> dtos) {
		List<PromotionEntity> entitys = new ArrayList<>();
		for(PromotionDto dto : dtos) {
			entitys.add(toEntity(dto));
		}
		return entitys;
	}

	@Override
	public List<PromotionDto> toDto(List<PromotionEntity> entitys) {
		List<PromotionDto> dtos = new ArrayList<>();
		for(PromotionEntity entity : entitys) {
			dtos.add(toDto(entity));
		}
		return dtos;
	}

}
