package myphamseoul.service;

import myphamseoul.base.ServiceBase;
import myphamseoul.entity.TagEntity;

public interface TagService extends ServiceBase<TagEntity, Long>{
	TagEntity findOneByName(String name);
}
