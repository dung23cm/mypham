package myphamseoul.service;

import myphamseoul.base.ServiceBase;
import myphamseoul.entity.LineProductEntity;

public interface LineProductService extends ServiceBase<LineProductEntity, Long> {
	LineProductEntity findOneByName(String name);
}
