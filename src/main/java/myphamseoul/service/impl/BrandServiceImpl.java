package myphamseoul.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import myphamseoul.entity.BrandEntity;
import myphamseoul.repository.BrandRepository;
import myphamseoul.service.BrandService;

@Service
public class BrandServiceImpl implements BrandService{
	
	@Autowired
	private BrandRepository brandRepository;

	@Override
	public List<BrandEntity> getAll() {
		return brandRepository.findAll();
	}

	@Override
	public BrandEntity getById(Long id) {
		return brandRepository.getOne(id);
	}

	@Override
	public BrandEntity save(BrandEntity entity) {
		return brandRepository.save(entity);
	}

	@Override
	public void delete(Long id) {
		brandRepository.deleteById(id);
	}

	@Override
	public void delete(Long[] ids) {
		for(Long id : ids) {
			delete(id);
		}
	}

	@Override
	public BrandEntity findOneByName(String name) {
		return brandRepository.findOneByNameIgnoreCase(name);
	}

	@Override
	public boolean existById(Long id) {
		return brandRepository.existsById(id);
	}

}
