package myphamseoul.service.impl;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import myphamseoul.dto.ColorDto;
import myphamseoul.dto.ProductDto;
import myphamseoul.dto.ProductMainDto;
import myphamseoul.dto.PromotionDto;
import myphamseoul.entity.ColorEntity;
import myphamseoul.entity.ImageEntity;
import myphamseoul.entity.ProductColorKey;
import myphamseoul.entity.ProductEntity;
import myphamseoul.entity.ProductMainEntity;
import myphamseoul.entity.ProductPromotionEntity;
import myphamseoul.entity.ProductPromotionKey;
import myphamseoul.entity.PromotionEntity;
import myphamseoul.mapper.ColorMapper;
import myphamseoul.mapper.PromotionMapper;
import myphamseoul.repository.ProductPromotionRepository;
import myphamseoul.repository.ProductRepository;
import myphamseoul.request.ProductFileRequest;
import myphamseoul.service.ColorService;
import myphamseoul.service.FileService;
import myphamseoul.service.ImageService;
import myphamseoul.service.ProductMainService;
import myphamseoul.service.ProductPromotionService;
import myphamseoul.service.ProductService;
import myphamseoul.service.PromotionService;

@Service
public class ProductServiceImpl implements ProductService {

	@Autowired
	private ProductMainService productMainService;

	@Autowired
	private ProductPromotionRepository productPromotionRepository;

	@Autowired
	private ProductRepository productRepository;

	@Autowired
	private PromotionService promotionService;

	@Autowired
	private ColorMapper colorMapper;

	@Autowired
	private ColorService colorService;

	@Autowired
	private PromotionMapper promotionMapper;

	@Autowired
	private ProductPromotionService productPromotionService;

	@Autowired
	private ImageService imageService;

	@Autowired
	private FileService fileService;

	@Override
	public List<ProductEntity> getAll() {
		return productRepository.findAll();
	}

	@Override
	public ProductEntity getById(ProductColorKey id) {
		return productRepository.findByProductMainIdAndColorId(id.getProductMainId(), id.getColorId());
	}

	@Override
	public ProductEntity save(ProductEntity entity) {
		return productRepository.save(entity);
	}

	@Override
	public boolean existById(ProductColorKey id) {
		return productRepository.existsById(id);
	}

	@Override
	public void delete(ProductColorKey id) {
		productRepository.deleteById(id);
	}

	@Override
	public void delete(ProductColorKey[] ids) {
		for (ProductColorKey id : ids) {
			delete(id);
		}
	}

	@Override
	public List<ProductEntity> findByProductMain(ProductMainEntity productMain) {
		return productRepository.findByProductMain(productMain);
	}

	@Override
	public List<ColorEntity> findColorByProductMain(ProductMainEntity productMain) {
		return productRepository.findColorByProductMain(productMain);
	}

	@Override
	public ProductEntity save(ProductFileRequest product) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<ProductEntity> findProductByPromotionIdMax(Long promotionId) {
		Date dateNow = new Date();
		return productPromotionRepository.findProductByPromotionMax(promotionService.getById(promotionId), dateNow);
	}

	@Override
	public ProductEntity save(ProductDto dto) {
		ProductMainDto proMainDto = dto.getProductMain();
		ProductEntity proE = null;
		if (proMainDto != null) {
			ProductMainEntity productmainE = null;
			if (proMainDto.getId() != null && !proMainDto.getId().trim().equals("")) {
				productmainE = productMainService.getById(proMainDto.getId());
				if (productmainE == null) {
					return null;
				}
			} else {
				productmainE = productMainService.findBySku(proMainDto.getSku());
				if (productmainE == null) {
					productmainE = productMainService.save(proMainDto);
					if (productmainE == null) {
						return null;
					}
				}

			}
			ColorDto colorDto = dto.getColor();
			ColorEntity colorE = null;
			if (colorDto != null) {
				if (colorDto.getId() != null && colorDto.getId() > 0) {
					colorE = colorService.getById(colorDto.getId());
				} else {
					colorE = colorService.findByNameAndCode(colorDto.getName(), colorDto.getColorCode());
					if (colorE == null) {
						colorE = colorService.save(colorMapper.toEntity(colorDto));
					}
				}
				if (colorE == null) {
					colorE = colorService.findByNameAndCode("không màu", "transparent");
					if (colorE == null) {
						colorE = colorService.save(new ColorEntity(null, "không màu", "transparent"));
					}
				}
				proE = new ProductEntity(new ProductColorKey(productmainE.getId(), colorE.getId()), productmainE,
						colorE, dto.getPrice());
			}
			if (dto.getImageIds().size() > 0) {
				for (String imgId : dto.getImageIds()) {
					if (!imageService.existById(imgId)) {
						ImageEntity imgE = imageService.getById(imgId);
						if (fileService.getContentTypeByFilename(imgE.getName()) != null) {
							imgE.setProduct(proE);
							imgE.setDraft(false);
							imageService.save(imgE);
						}else {
							imageService.delete(imgId);
						}
					}
				}
			}
			PromotionDto promDto = dto.getPromotion();
			PromotionEntity promE = null;
			if (promDto != null) {
				if (promDto.getId() != null && promDto.getId() > 0) {
					promE = promotionService.getById(promDto.getId());
				} else {
					promE = promotionService.save(promotionMapper.toEntity(promDto));
				}
				if (promE != null && proE != null) {
					productPromotionService.save(new ProductPromotionEntity(
							new ProductPromotionKey(proE.getId(), promE.getId()), proE, promE));
				}
			}

		}
		return save(proE);
	}

}
