package myphamseoul.service;

import java.util.List;

import myphamseoul.base.ServiceBase;
import myphamseoul.dto.ProductDto;
import myphamseoul.entity.ColorEntity;
import myphamseoul.entity.ProductColorKey;
import myphamseoul.entity.ProductEntity;
import myphamseoul.entity.ProductMainEntity;
import myphamseoul.request.ProductFileRequest;

public interface ProductService extends ServiceBase<ProductEntity, ProductColorKey>{
	List<ProductEntity> findByProductMain(ProductMainEntity productMain);
	List<ColorEntity> findColorByProductMain(ProductMainEntity productMain);
	ProductEntity save(ProductFileRequest product);
	List<ProductEntity> findProductByPromotionIdMax(Long promotionId);
	ProductEntity save(ProductDto dto);
}
