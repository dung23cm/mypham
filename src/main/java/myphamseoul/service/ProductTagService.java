package myphamseoul.service;

import java.util.List;

import myphamseoul.base.ServiceBase;
import myphamseoul.dto.ProductTagDto;
import myphamseoul.entity.ProductMainEntity;
import myphamseoul.entity.ProductTagEntity;
import myphamseoul.entity.ProductTagkey;
import myphamseoul.entity.TagEntity;

public interface ProductTagService extends ServiceBase<ProductTagEntity, ProductTagkey>{
	List<ProductMainEntity> findProductMainByTagId(Long tagId);
	List<TagEntity> findProductMainByProductMainId(String productMainId);
	ProductTagEntity findByProductMainIdAndTagId(String productMainId,Long tagId);
	ProductTagEntity save(ProductTagDto tagD);
}
