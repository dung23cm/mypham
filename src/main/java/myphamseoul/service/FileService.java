package myphamseoul.service;

import java.io.File;

import org.springframework.core.io.Resource;
import org.springframework.web.multipart.MultipartFile;

public interface FileService {
	String getDir();
	String save(MultipartFile file);
	Resource getResource(String filename);
	String getContentTypeByFilename(String filename);
	boolean deleteByFilename(String filename);
	File getFileByFilename(String filename);
}
