package myphamseoul.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import myphamseoul.entity.PromotionEntity;

public interface PromotionRepository extends JpaRepository<PromotionEntity, Long>{

}
