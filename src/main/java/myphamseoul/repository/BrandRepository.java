package myphamseoul.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import myphamseoul.entity.BrandEntity;

public interface BrandRepository extends JpaRepository<BrandEntity, Long> {
	BrandEntity findOneByNameIgnoreCase(String name);
}
