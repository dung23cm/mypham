package myphamseoul.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import myphamseoul.entity.LineProductEntity;

public interface LineProductRepository extends JpaRepository<LineProductEntity, Long>{
	LineProductEntity findByNameIgnoreCase(String name);
}
