package myphamseoul.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import myphamseoul.entity.ColorEntity;
import myphamseoul.entity.ProductColorKey;
import myphamseoul.entity.ProductEntity;
import myphamseoul.entity.ProductMainEntity;

public interface ProductRepository extends JpaRepository<ProductEntity, ProductColorKey>{
	List<ProductEntity> findByProductMain(ProductMainEntity productMain);
	@Query(value = "select p.color from ProductEntity p where p.productMain = ?1")
	List<ColorEntity> findColorByProductMain(ProductMainEntity productMain);
	ProductEntity findByProductMainIdAndColorId(String productMainId,Long colorId);
}
