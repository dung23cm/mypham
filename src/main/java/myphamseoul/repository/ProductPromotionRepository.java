package myphamseoul.repository;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import myphamseoul.entity.ProductEntity;
import myphamseoul.entity.ProductMainEntity;
import myphamseoul.entity.ProductPromotionEntity;
import myphamseoul.entity.ProductPromotionKey;
import myphamseoul.entity.PromotionEntity;

public interface ProductPromotionRepository extends JpaRepository<ProductPromotionEntity, ProductPromotionKey>{
	@Query(value = "select pp.promotion from ProductPromotionEntity pp where pp.product = ?1")
	List<PromotionEntity> findPromotionByProduct(ProductEntity product);

	ProductPromotionEntity findByProductProductMainIdAndProductColorIdAndPromotionId(String productMainId, Long colorId,
			Long promotionId);
	
	List<ProductEntity> findProductByPromotionId(Long promotionId);
	@Query(value = "select pp.promotion from ProductPromotionEntity pp where pp.product.productMain = ?1 "
			+ "and pp.promotion.locationIn <= ?2 "
			+ "and pp.promotion.locationOut > ?2 order by pp.promotion.sale desc")
	List<PromotionEntity> findPromotionByProductMain(ProductMainEntity productMain,Date dateNow);
	@Query(value = "select pp.product from ProductPromotionEntity pp where pp.promotion = ?1 "
			+ "and pp.promotion.locationIn <= ?2 "
			+ "and pp.promotion.locationOut > ?2 order by pp.promotion.sale desc")
	List<ProductEntity> findProductByPromotionMax(PromotionEntity promotion,Date dateNow);
}
