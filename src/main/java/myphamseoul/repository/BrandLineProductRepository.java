package myphamseoul.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import myphamseoul.entity.BrandEntity;
import myphamseoul.entity.BrandLineProductEntity;
import myphamseoul.entity.BrandLineProductKey;
import myphamseoul.entity.LineProductEntity;

public interface BrandLineProductRepository extends JpaRepository<BrandLineProductEntity, BrandLineProductKey> {
	@Query(value = "select blp.lineProduct from BrandLineProductEntity blp where blp.brand = ?1")
	List<LineProductEntity> findByBrand(BrandEntity brand);
	@Query(value = "select blp.brand from BrandLineProductEntity blp where blp.lineProduct = ?1")
	List<BrandEntity> findByLineProduct(LineProductEntity lineProduct);
	BrandLineProductEntity findByBrandIdAndLineProductId(Long brandId,Long lineProductId);
}
